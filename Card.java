public class Card{
	//Two fields for a card: suits and values
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		//initializing the values of the field using the parameters
		this.suit = suit;
		this.value = value;
	}
	public String getSuit(){
		// returns the suit of the card
		return this.suit;
	}
	public String getValue(){
		// returns the value of the card
		return this.value;
	}
	public String toString(){
		//changes the output of printing a card.
		return this.value + " of " + this.suit;
	}
	public double calculateScore(){
		//Calculates the score for a card by taking the value as the number before the decimal and the suits as the points after the decimal for tiebreakers.
		String[] suits = new String[] {"Spades", "Clubs", "Diamonds", "Hearts"};
		String[] values = new String[] {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		double pointsForValues = 0.0;	//to calculate the points for what value the card is.
		double pointsForSuits = 0.0;	//to calculate the points for what suit the card is.
		for(int i = 0; i < values.length; i++){		//loops over the values from lowest to greatest and gives points respectingly to the value of the card
			if(this.value.equals(values[i])){
				pointsForValues = i+1;
			}
		}
		for(int i = 0; i < suits.length; i++){		//loops over the suits from lowest to greatest and gives points respectingly to the suit of the card
			if(this.suit.equals(suits[i])){
				pointsForSuits= (Double.valueOf(i)+1)/10;
			}
		}
		return pointsForValues+pointsForSuits;	//adds the points for both value and suit and returns the final score for the round.
	}
}
public class SimpleWar{
	public static void main(String[] args){
		Deck drawPile = new Deck();
		drawPile.shuffle();
		int firstPlayerPoints = 0;		//points for player 1
		int secondPlayerPoints = 0;		//points for player 2
		int roundNumber = 1;			//used to display roud number
		while(drawPile.length() > 0){
			Card player1Card = drawPile.drawTopCard();		//player 1's card
			Card player2Card = drawPile.drawTopCard();		//player 2's card
			//says the round number and adds 1 to the variable for next round.
			System.out.println("ROUND: " + roundNumber + "\n\nPlayer 1's card: " + player1Card);
			roundNumber++;
			
			//Tells the Player 1's score, second player's card and their score for the round.
			System.out.println("Player 1's score for their card: " + player1Card.calculateScore() + "\n");
			System.out.println("Player 2's card: " + player2Card);
			System.out.println("Player 2's score for their card: " + player2Card.calculateScore() + "\n");
			
			//Calculates the score for both players' cards, compares them, and tells the user which player won the round.
			if(player1Card.calculateScore() > player2Card.calculateScore()){
				firstPlayerPoints++;
				System.out.println("Player 1 wins this round and gains 1 point\nPlayer 1's points: "+ firstPlayerPoints + "\n--------------------------------------------------------------\n");
			}
			else{
				secondPlayerPoints++;
				System.out.println("Player 2 wins this round and gains 1 point\nPlayer 2's points: "+ secondPlayerPoints + "\n--------------------------------------------------------------\n");
			}
		}
		//Checks the final score of both players and congrats the winner and makes fun of the loser or tells whether the players tied.
		if(firstPlayerPoints > secondPlayerPoints){
			System.out.println("Congrats PLAYER 1, you won with " + firstPlayerPoints + " points\nThe second player lost with " + secondPlayerPoints + " points. Skill issue.");
		}
		else{
			if(firstPlayerPoints == secondPlayerPoints){
				System.out.println("Both players tied with " + firstPlayerPoints + " points");
			}
			else{
				System.out.println("Congrats PLAYER 2, you won with " + secondPlayerPoints + " points\nThe first player lost with " + firstPlayerPoints + " points. Skill issue.");
			}
		}
	}
}